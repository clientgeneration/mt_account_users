MtAccountUsers::Engine.routes.draw do
	resources :users do
		member do
			get 'activate'
			get 'deactivate'
		end
		collection do
			get 'import/new' => 'users#import_new'
			post 'import' => 'users#import_create'
			get 'import/summary' => 'users#import_summary'
		end
	end
end
