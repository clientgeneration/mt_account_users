module MtAccountUsers
  module UsersHelper
  	def gravatar128 email
		# include MD5 gem, should be part of standard ruby install
		# require 'digest/md5'
		 
		# get the email from URL-parameters or what have you and make lowercase
		email_address = email.downcase
		 
		# create the md5 hash
		hash = Digest::MD5.hexdigest(email_address)
		 
		# compile URL which can be used in <img src="RIGHT_HERE"...
		image_src = "http://www.gravatar.com/avatar/#{hash}?s=128&d=identicon"

		image_tag(image_src, class: "img-responsive")
	end

	def avatar user
		begin
			if !user.avatar.blank?
				return image_tag(user.avatar.url, class: "img-responsive")
			else
				# include MD5 gem, should be part of standard ruby install
				# require 'digest/md5'
				 
				# get the email from URL-parameters or what have you and make lowercase
				email_address = user.email.downcase
				 
				# create the md5 hash
				hash = Digest::MD5.hexdigest(email_address)
				 
				# compile URL which can be used in <img src="RIGHT_HERE"...
				image_src = "http://www.gravatar.com/avatar/#{hash}?s=128&d=identicon"

				return image_tag(image_src, class: "img-responsive")
			end
		rescue
			# include MD5 gem, should be part of standard ruby install
			# require 'digest/md5'
			 
			# get the email from URL-parameters or what have you and make lowercase
			email_address = user.email.downcase
			 
			# create the md5 hash
			hash = Digest::MD5.hexdigest(email_address)
			 
			# compile URL which can be used in <img src="RIGHT_HERE"...
			image_src = "http://www.gravatar.com/avatar/#{hash}?s=128&d=identicon"

			return image_tag(image_src, class: "img-responsive")
		end
	end
  end
end
