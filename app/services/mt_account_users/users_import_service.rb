module MtAccountUsers
  class UsersImportService
	def initialize import_file, default_password, default_tags
		@import_file = import_file
		@default_password = default_password
		@default_tags = default_tags

		self.file_error_messages = []
		self.params_error_messages = []
		self.import_error_messages = []
		self.success_import_count = 0
		self.failed_import_count = 0
	end


	attr_accessor :file_error_messages, :params_error_messages, :import_error_messages, :success_import_count, :failed_import_count

	def import_params_valid?
		if @import_file.blank?
			self.params_error_messages << "CSV file is not present."
		else
			self.params_error_messages << "Import file is not in CSV file format." if @import_file.content_type != "text/csv"
		end
		
		if @default_password.blank?
			self.params_error_messages << "Please define a default password for your users."
		else
			self.params_error_messages << "Password must be at least 8 characters long." if @default_password.length < 8
		end
		
		if self.params_error_messages.length == 0
			return true
		else
			return false
		end
	end

	def import_file_valid?
		begin
			csv_import_file = CSV.parse(File.read(@import_file.path, :encoding => 'windows-1251:utf-8'))
			self.file_error_messages << "Import file does not have 'email' in the header row." if !csv_import_file[0].include? "email"
			self.file_error_messages << "Import file does not have 'first_name' in the header row." if !csv_import_file[0].include? "first_name"
			self.file_error_messages << "Import file does not have 'last_name' in the header row." if !csv_import_file[0].include? "last_name"
			self.file_error_messages << "Import file must contain only the 3 required fields." if csv_import_file[0].length != 3

			if self.file_error_messages.length == 0
				return true
			else
				return false
			end
		rescue
			self.file_error_messages << "Something went wrong when parsing the CSV file. Please contact the site owner."
			return false
		end
	end

	def create_users current_user, ip_address
		begin
			csv_import_file = CSV.parse(File.read(@import_file.path, :encoding => 'windows-1251:utf-8'), headers: true)
		rescue
			self.file_error_messages = [] # clear the array, it should be empty anyway
			self.file_error_messages << "Something went wrong when parsing the CSV file. Please contact the site owner."
			return false
		end

		csv_import_file.each_with_index do |row, i|
			begin
				user = User.new(row.to_hash)
				user.account_id = current_user.id
				user.created_by = current_user.id
				user.created_ip = ip_address
				user.updated_by = current_user.id
				user.updated_ip = ip_address
				user.password = @default_password

				if user.save
					current_user.account.tag(user, :with => @default_tags, :on => :tags)
					self.success_import_count += 1
				else
					self.failed_import_count += 1
					self.import_error_messages << [i+2,user.errors.full_messages]
				end
			rescue
				self.failed_import_count += 1
				self.import_error_messages << [i+2, ["Data is not formatted properly."]]
			end
		end
	end
  end
end
