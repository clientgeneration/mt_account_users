module MtAccountUsers
  class ApplicationController < ActionController::Base
  	def current_tenant
		@tenant = Tenant.new(current_user)
	end
	helper_method :current_tenant

	def authenticate_admin!
		if user_signed_in?
			render 'shared/errors/authorization_error' unless current_user.user_type == "admin"
		end
	end
  end
end
