module MtAccountUsers
  class UsersController < ApplicationController
  	before_action :authenticate_user!
  	before_action :authenticate_admin! if MtAccountUsers.mt_account_users_admin_only_feature == true
  	layout MtAccountUsers.mt_account_users_layout_name

  	def index
  		@users = current_tenant.users
  		@users = @users.tagged_with(params[:tag], :on => :tags, :owned_by => current_tenant.account) if !params[:tag].blank?
  		respond_to do |format|
			format.html { @users = @users.decorate }
			format.csv { send_data to_csv(@users) }
		end
  	end

  	def show
  		@user = current_tenant.users.find(params[:id]).decorate
  	end

 	def new
  		@user = current_tenant.users.new
  	end

  	def edit
  		@user = current_tenant.users.find(params[:id])
  		@tags = current_tenant.tags
  	end

  	def create
		@user = current_tenant.users.new(user_params)
		if @user.save
			current_tenant.account.tag(@user, :with => params[:user][:tag_list], :on => :tags)
			redirect_to @user, notice: 'User was successfully created.' 
		else
			render action: "new"
		end
	end

	def update
		@user = current_tenant.users.find(params[:id])
		if @user.update_attributes(user_params)
			current_tenant.account.tag(@user, :with => params[:user][:tag_list], :on => :tags)
			redirect_to @user, notice: 'User was successfully updated.'
		else 
			render action: "edit" 
		end
	end

	def activate
		@user = current_tenant.users.find(params[:id])
		@user.update(active: true)

		redirect_to :back, notice: 'User was successfully activated'
	end

	def deactivate
		@user = current_tenant.users.find(params[:id])
		@user.update(active: false)

		redirect_to :back, notice: 'User was successfully deactivated'
	end

	def import_new
		render '/mt_account_users/users/import/new'
	end

	def import_create
		user_import_service =  UsersImportService.new(params[:users_csv], params[:password], params[:tag_list])
		if user_import_service.import_params_valid?
			if user_import_service.import_file_valid?
				user_import_service.create_users(current_user, request.remote_ip)
				@import_error_messages = user_import_service.import_error_messages
				@success_import_count = user_import_service.success_import_count
				@failed_import_count = user_import_service.failed_import_count
				render '/mt_account_users/users/import/summary'
			else
				@import_error_messages = user_import_service.file_error_messages
				render '/mt_account_users/users/import/new'
			end
		else
			@import_error_messages = user_import_service.params_error_messages
			render '/mt_account_users/users/import/new'
		end
	end

  	private
	def user_params
		if params[:action] == 'create'
			params[:user][:account_id]=current_user.account_id
			params[:user][:created_by]=current_user.id
			params[:user][:created_ip]=request.remote_ip
		end
		# handle "leave password blank if you do not want to change"
		if params[:action] == 'update'
			params[:user].delete :password if params[:user][:password].blank?
			params[:user].delete :password_confirmation if params[:user][:password_confirmation].blank?
		end
		params[:user][:updated_by]=current_user.id
		params[:user][:updated_ip]=request.remote_ip

		params.require(:user).permit(:email, :first_name, :last_name, :user_type, :password, :password_confirmation, :account_id, :created_by, :created_ip, :updated_by, :updated_ip)  
	end

	private
	def to_csv users
		CSV.generate do |csv|
		csv << ["Email", "First name", "Last name"]
			users.each do |user|
				csv << [user.email, user.first_name, user.last_name]
			end
		end
	end
  end
end
