$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "mt_account_users/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "mt_account_users"
  s.version     = MtAccountUsers::VERSION
  s.authors     = ["Hong Lance Lui"]
  s.email       = ["honglui@multiplier.io"]
  s.homepage    = "http://multiplier.io"
  s.summary     = "Manage users in your account"
  s.description = "Manage users in your account."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.1"
  s.add_dependency "simple_form"
  s.add_dependency "devise"
  s.add_dependency "draper"
  s.add_dependency "acts-as-taggable-on"

  s.add_development_dependency "sqlite3"
end
