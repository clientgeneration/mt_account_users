MtAccountUsers.setup do |config|
	# make the entire engine available only if user.user_type == 'admin'
    # config.mt_account_users_admin_only_feature = true

    # allow users to edit every user's password
    # config.mt_account_users_admin_edit_password = false

    # change your layout name
    # config.mt_account_users_layout_name = "application"

    # to display action buttons on panel or not
    # some layout like topmenu does not need this
    # config.mt_account_users_panel_action_buttons = false

    # user type list to allow user type editing
    # config.mt_account_users_user_type = ['admin', 'normal', 'user']

    # a list of additional attributes you want to display in show page
    # for situations where methods are called in two levels, use decorator methods like "documents_count" instead of "documents.count"
    # config.mt_account_users_extra_show_details = [
    #     {"label" => "Documents", "attribute" => "documents.count"}
    # ]
end