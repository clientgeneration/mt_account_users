module MtAccountUsers
  module Generators
    class InstallGenerator < Rails::Generators::Base
        # this line defines the source path, defaults to parent app
        source_root File.expand_path("../../templates", __FILE__)

        def add_route
            route "mount MtAccountUsers::Engine => '/account'"
        end

        def create_initializer_file
            copy_file '../../templates/mt_account_users.rb', 'config/initializers/mt_account_users.rb'
        end

        # def copy_stylesheets
        #     puts "This gem has some stylesheet assets. Copy stylesheets? [y/n]"
        #     if gets.chomp == 'y'
        #         copy_file '../../../app/assets/stylesheets/item_engine/item.css', 'app/assets/stylesheets/item_engine/item.css.scss'
        #     else
        #         puts "\033[31mCSS assets not created, some default styling will be absent\033[0m"
        #     end
        # end

        # def copy_javascripts
        #     puts "This gem has some javascript assets. Copy javascripts? [y/n]"
        #     if gets.chomp == 'y'
        #         copy_file '../../../app/assets/javascripts/item_engine/item.js', 'app/assets/javascripts/item_engine/item.js'
        #     else
        #         puts "\033[31mJavascript assets not created, some javascript functionality will not work\033[0m"
        #     end
        # end

        def requirements
            puts "\n============================================================"
            puts "Requirements"
            puts "============================================================"
            puts "1. Must have current_tenant, user and account defined"
            puts "2. Have decorator for users \n\n"
        end
    end
  end
end