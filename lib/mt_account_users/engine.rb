module MtAccountUsers
  class Engine < ::Rails::Engine
    isolate_namespace MtAccountUsers
  end

  # these code sets variable from initializer
  class << self
      mattr_accessor :mt_account_users_admin_edit_password, :mt_account_users_admin_only_feature, :mt_account_users_layout_name, :mt_account_users_panel_action_buttons, :mt_account_users_user_type, :mt_account_users_has_many_list, :mt_account_users_extra_show_details
      self.mt_account_users_admin_edit_password = false
      self.mt_account_users_admin_only_feature = true
      self.mt_account_users_layout_name = "application"
      self.mt_account_users_panel_action_buttons = true
      self.mt_account_users_user_type = ["user","admin"]
      self.mt_account_users_has_many_list = []
      self.mt_account_users_extra_show_details = []
      # add default values of more config vars here
  end

   # this function maps the vars from your app into your engine
   def self.setup(&block)
      yield self
   end
end
